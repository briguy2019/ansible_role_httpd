About
-----

Ansible role to install and configure apache on a linux system.

Variables
---------

httpd_enable_ssl: false

httpd_access_log:

httpd_access_log_ssl:

httpd_document_root:

httpd_error_log:

httpd_error_log_ssl:

httpd_extended_status:

httpd_listen:

httpd_listen_ssl:

httpd_log_level:

httpd_log_level_ssl:

httpd_server_admin:

httpd_server_root:

httpd_server_tokens:

httpd_ssl_certificate_file:

httpd_ssl_certificate_key_file:

httpd_status_enable:

httpd_status_location

httpd_status_require:

httpd_ssl_cipher_suite:

httpd_ssl_compression:

httpd_ssl_honor_cipher_order:

httpd_ssl_protocol:

httpd_ssl_session_tickets:

httpd_ssl_stapling_responder_timeout:

httpd_ssl_stapling_return_responder_errors:

httpd_ssl_stapling_cache:

httpd_ssl_use_stapling:


Usage Example
-------------

```yml
httpd_access_log: logs/access_log
httpd_access_log_ssl: logs/ssl_access_log
httpd_document_root: '/var/www/html'
httpd_error_log: logs/error_log
httpd_error_log_ssl: logs/ssl_error_log
httpd_extended_status: 'On'
httpd_listen: 80
httpd_listen_ssl: 443
httpd_log_level: warn
httpd_log_level_ssl: warn
httpd_server_admin: root@localhost
httpd_server_root: '/etc/httpd'
httpd_server_tokens: Prod
httpd_ssl_certificate_file: localhost.crt
httpd_ssl_certificate_key_file: localhost.key
httpd_status_enable: false
httpd_status_location: '/server-status'
httpd_status_require: 'host localhost'
httpd_ssl_cipher_suite: >
  'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256'
httpd_ssl_compression: 'off'
httpd_ssl_honor_cipher_order: 'on'
httpd_ssl_protocol: 'all -SSLv3 -TLSv1 -TLSv1.1'
httpd_ssl_session_tickets: 'off'
httpd_ssl_stapling_responder_timeout: 5
httpd_ssl_stapling_return_responder_errors: 'off'
httpd_ssl_stapling_cache: 'shmcb:/var/run/ocsp(128000)'
httpd_ssl_use_stapling: 'on'
roles:
  - { role: ansible_role_httpd, tags: web }
```


Testing
-------

```bash
molecule test
```
